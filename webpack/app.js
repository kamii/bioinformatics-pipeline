/*
    Created using PyCharm
    Author: Kamran Akram
    Email: kamranakram1000@live.com
    Date: 3/9/19
*/


import './js/pace';

/************ Import Libraries ************/

import 'bootstrap'; // Import Bootstrap JS

import '@fortawesome/fontawesome-free/js/all';

import 'select2';
import 'select2/dist/css/select2.css';

import 'animate.css/animate.min.css';

import './scss/app.scss';

import './js/custom'
