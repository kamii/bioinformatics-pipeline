/*
    Created using PyCharm
    Author: Kamran Akram
    Email: kamranakram1000@live.com
    Date: 6/9/19
*/

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover({
        html: true,
        container: 'body',
        title: function () {
            return $(this).attr('data-title') + '<span class="close-popover float-right btn btn-link p-0">' +
                '<i class="fas fa-times"></i>' +
                '</span>';
        },
    });
    $(document).on('click', '.close-popover', function () {
        $(this).parent().closest('.popover').popover('hide');
    });
    $('.select2').select2();

    $('.select2-search--hide').select2({
        minimumResultsForSearch: Infinity
    });

    $('#output_copy_to_clipboard').click(function () {
        if (CopyText('output_data')) {
            $('#output_copy_to_clipboard').html("Copied");
            $('#output_copy_to_clipboard').addClass("animated flash infinite");
            setTimeout(function () {
                $('#output_copy_to_clipboard').html("Copy");
                $('#output_copy_to_clipboard').removeClass("animated flash infinite");
            }, 1000);
        }
    });
    
    $('#sidebar-toggler').click(function () {
        if($('#sidebar-menu').hasClass('d-none')){
            $('#sidebar-menu').removeClass('d-none');
            $('#main-content').addClass('col-lg-9');
            $('#main-content').removeClass('col-lg-12');
            document.getElementById('body-wrapper').scrollIntoView();
        } else {
            $('#sidebar-menu').addClass('d-none');
            $('#main-content').removeClass('col-lg-9');
            $('#main-content').addClass('col-lg-12');
        }
    });
});

window.nl2br = function (str, is_xhtml) {
    if (typeof str === 'undefined' || str === null) {
        return '';
    }
    let breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
};

window.CopyText = function (element) {
    let doc = document
        , text = doc.getElementById(element)
        , range, selection
    ;
    if (doc.body.createTextRange) {
        range = document.body.createTextRange();
        range.moveToElementText(text);
        range.select();
    } else if (window.getSelection) {
        selection = window.getSelection();
        range = document.createRange();
        range.selectNodeContents(text);
        selection.removeAllRanges();
        selection.addRange(range);
    }
    let copySuccess = true;
    try {
        document.execCommand("copy") // run command to copy selected text to clipboard
    } catch (e) {
        alert("Copy to clipboard not supported by browser.")
        copySuccess = false;
    }
    return copySuccess;
};
