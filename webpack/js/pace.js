/*
    Created using PyCharm
    Author: Kamran Akram
    Email: kamranakram1000@live.com
    Date: 14/9/19
*/

// import pace.js
window.paceOptions = {
    ajax: {
        trackMethods: ['POST', 'GET']
    }
};
import 'pace-js/themes/blue/pace-theme-minimal.css';
const pace = require('pace-js');
pace.start();