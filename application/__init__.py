# Created using PyCharm
# Author: Kamran Akram
# Email: kamranakram1000@live.com
# Date: 9/9/19

from flask import Flask, g
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv

load_dotenv()

db = SQLAlchemy()
login_manager = LoginManager()
migrate = Migrate()


def initialize_flask_app():
    # Initialize the core application.
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object('config.Config')
    return app


def initialize_plugins(app):
    # Initialize Plugins
    db.init_app(app)
    login_manager.init_app(app)
    migrate.init_app(app, db)


def import_entities():
    # Import Database Models
    from application.entity import Users, Analysis


def import_jinja2filters():
    # Import Filters for Jinja2
    from application.utilities import jinja2filters


def register_blueprints(app):
    # Register Blueprints
    from application.controller import default, user_management, alignment_sequence_tools, phylogenetic_analysis_tools
    app.register_blueprint(default.controller)
    app.register_blueprint(user_management.controller)
    app.register_blueprint(alignment_sequence_tools.controller)
    app.register_blueprint(phylogenetic_analysis_tools.controller)


def create_app():
    app = initialize_flask_app()
    initialize_plugins(app)

    with app.app_context():
        # Inject parameters.yaml (path from .env) in templates with app prefix. e.g "app.variable_name"
        @app.context_processor
        def inject_parameters():
            from application.utilities import general
            parameters = general.get_parameters()
            app_parameters = {
                "app": {}
            }
            for key, value in parameters.items():
                app_parameters["app"][key] = value
            return app_parameters

        # Register blueprints
        register_blueprints(app)

        # Import Entities
        import_entities()

        # Import Filters for Jinja2
        import_jinja2filters()

        return app
