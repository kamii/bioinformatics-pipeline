# Created using PyCharm
# Author: Kamran Akram
# Email: kamranakram1000@live.com
# Date: 15/9/19
import datetime
import re

import timeago
from flask import current_app
from jinja2 import evalcontextfilter, Markup, escape


@current_app.template_filter()
@evalcontextfilter
def nl2br(eval_ctx, value):
    _paragraph_re = re.compile(r'(?:\r\n|\r|\n){2,}')
    result = u'\n\n'.join(u'<p>%s</p>' % p.replace('\n', Markup('<br>\n'))
                          for p in _paragraph_re.split(escape(value)))
    if eval_ctx.autoescape:
        result = Markup(result)
    return result


@current_app.template_filter()
def time_elapsed(value):
    now = datetime.datetime.now()
    return timeago.format(value, now)
