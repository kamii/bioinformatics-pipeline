# Created using PyCharm
# Author: Kamran Akram
# Email: kamranakram1000@live.com
# Date: 14/9/19


class AnalysisStatus:
    PENDING = 0
    IN_PROGRESS = 1
    COMPLETED = 2
    FAILED = -1


class AnalysisTypes:
    PROBCONS = "PROBCONS"
    MUSCLE = "MUSCLE"
    MAFFT = "MAFFT"
    DNAPARS = "DNAPARS"
    DNAML = "DNAML"
    PROTPARS = "PROTPARS"
    PROML = "PROML"
