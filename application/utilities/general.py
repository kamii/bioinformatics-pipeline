# Created using PyCharm
# Author: Kamran Akram
# Email: kamranakram1000@live.com
# Date: 3/9/19
import os
from datetime import datetime

from yaml import load, dump

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


def load_yaml(path):
    parameters_data = open(os.getcwd() + "/" + path, "r")
    return load(parameters_data, Loader=Loader)


def get_parameters():
    return load_yaml(os.environ.get('PARAMETER_FILE'))


def get_current_datetime_for_db():
    now = datetime.now()
    return now.strftime("%Y-%m-%d %H:%M:%S")
