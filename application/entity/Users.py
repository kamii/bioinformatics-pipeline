# Created using PyCharm
# Author: Kamran Akram
# Email: kamranakram1000@live.com
# Date: 9/9/19

from application import db
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash


class Users(UserMixin, db.Model):
    id = db.Column(db.Integer,
                   primary_key=True)

    email = db.Column(db.String(120),
                      unique=True,
                      nullable=False)

    password = db.Column(db.String(120),
                         unique=False,
                         nullable=False)

    first_name = db.Column(db.String(120),
                           unique=False,
                           nullable=True)

    last_name = db.Column(db.String(120),
                          unique=False,
                          nullable=True)

    created_on = db.Column(db.DateTime,
                           index=False,
                           unique=False,
                           nullable=False)

    last_login = db.Column(db.DateTime,
                           index=False,
                           unique=False,
                           nullable=True)

    analysis = db.relationship('Analysis', backref='analysis', lazy=True)

    def set_password(self, password):
        """Create hashed password."""
        self.password = generate_password_hash(password, method='sha256')

    def check_password(self, password):
        """Check hashed password."""
        return check_password_hash(self.password, password)

    def __repr__(self):
        return '<User {}>'.format(self.id)
