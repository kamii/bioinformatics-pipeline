# Created using PyCharm
# Author: Kamran Akram
# Email: kamranakram1000@live.com
# Date: 9/9/19

from application import db


class Analysis(db.Model):
    id = db.Column(db.Integer,
                   primary_key=True)

    type = db.Column(db.String(120),
                     unique=False,
                     nullable=False)

    status = db.Column(db.Integer,
                       unique=False,
                       nullable=False)

    input = db.Column(db.Text(4294000000),
                      unique=False,
                      nullable=False)

    output = db.Column(db.Text(4294000000),
                       unique=False,
                       nullable=True)

    created_on = db.Column(db.DateTime,
                           unique=False,
                           nullable=False)

    hash = db.Column(db.String(120),
                     unique=True,
                     nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'),
                        nullable=True)

    def __repr__(self):
        return '<id {}>'.format(self.id)
