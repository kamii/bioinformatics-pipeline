# Created using PyCharm
# Author: Kamran Akram
# Email: kamranakram1000@live.com
# Date: 3/9/19
from flask import Blueprint, render_template, redirect, url_for
from flask_login import login_required, current_user

from application import db
from application.entity.Analysis import Analysis

controller = Blueprint('default', __name__, template_folder='templates', url_prefix="/")


@controller.route('/')
def index():
    return render_template('default/index.html')


@controller.route('/list-analysis')
def list_analysis():
    if current_user.is_authenticated:
        user_id = current_user.get_id()
        analysis = db.session.query(Analysis).filter_by(user_id=user_id).all()
        return render_template('default/list_analysis.html',
                               analysis=analysis)
    else:
        return redirect(url_for('user_management.login'))