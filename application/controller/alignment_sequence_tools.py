# Created using PyCharm
# Author: Kamran Akram
# Email: kamranakram1000@live.com
# Date: 5/9/19
import hashlib
import os
import random
import subprocess
from time import time

from flask import Blueprint, render_template, request, jsonify
from flask_login import current_user

from application import db
from application.entity.Analysis import Analysis
from application.utilities import general
from application.utilities.analysis import AnalysisStatus, AnalysisTypes

controller = Blueprint('alignment', __name__, template_folder='templates', url_prefix="/alignment")


@controller.route('/probcons')
def probcons():
    return render_template('alignment/probcons.html')


@controller.route('/probcons-analysis', methods=["POST"])
def probcons_analysis():
    input_data = request.form['input_data']
    output_format = request.form['output_format']
    consistency_reps = request.form['consistency_reps']
    iterative_refinement_reps = request.form['iterative_refinement_reps']
    pre_training_reps = request.form['pre_training_reps']
    output_data = ""
    analysis_status = True

    analysis = Analysis()
    analysis.input = input_data
    analysis.status = AnalysisStatus.IN_PROGRESS
    analysis.type = AnalysisTypes.PROBCONS
    analysis.created_on = general.get_current_datetime_for_db()
    if current_user.is_authenticated:
        analysis.user_id = current_user.get_id()

    hash_text = str(time()) + "-" + str(random.randint(0, 100000))
    analysis.hash = hashlib.sha224(hash_text.encode('utf-8')).hexdigest()

    db.session.add(analysis)
    db.session.commit()

    input_file_path = os.environ.get('TEMP_FILES_PATH') + "/" + analysis.hash + ".mfa"
    output_file_path = os.environ.get('TEMP_FILES_PATH') + "/" + analysis.hash + ".aln"
    f = open(input_file_path, "w+")
    f.write(input_data)
    f.close()

    command_to_execute = "analysis_tools/alignment/probcons/probcons"
    if output_format == "clustalw":
        command_to_execute += " -clustalw"
    command_to_execute += " -c " + str(int(consistency_reps))
    command_to_execute += " -ir " + str(int(iterative_refinement_reps))
    command_to_execute += " -pre " + str(int(pre_training_reps))
    command_to_execute += " " + str(input_file_path) + " > " + str(output_file_path)
    result = subprocess.run([command_to_execute], stdout=subprocess.PIPE, shell=True)
    try:
        if int(result.returncode) == 0:
            f = open(output_file_path, "r")
            output_data = f.read()
            analysis.output = output_data
            analysis.status = AnalysisStatus.COMPLETED
            db.session.add(analysis)
            db.session.commit()
        else:
            analysis.status = AnalysisStatus.FAILED
            db.session.add(analysis)
            db.session.commit()
            analysis_status = False
    except Exception as e:
        analysis.status = AnalysisStatus.FAILED
        db.session.add(analysis)
        db.session.commit()
        analysis_status = False

    os.remove(input_file_path)
    os.remove(output_file_path)

    response = {
        'status': analysis_status,
        'output_data': output_data
    }
    return jsonify(**response)


@controller.route('/muscle')
def muscle():
    return render_template('alignment/muscle.html')


@controller.route('/muscle-analysis', methods=["POST"])
def muscle_analysis():
    input_data = request.form['input_data']
    output_format = request.form['output_format']
    output_data = ""
    maketree = request.form['maketree']
    maketree_format = request.form['maketree_format']
    analysis_status = True

    analysis = Analysis()
    analysis.input = input_data
    analysis.status = AnalysisStatus.IN_PROGRESS
    analysis.type = AnalysisTypes.MUSCLE
    analysis.created_on = general.get_current_datetime_for_db()
    if current_user.is_authenticated:
        analysis.user_id = current_user.get_id()

    hash_text = str(time()) + "-" + str(random.randint(0, 100000))
    analysis.hash = hashlib.sha224(hash_text.encode('utf-8')).hexdigest()

    db.session.add(analysis)
    db.session.commit()

    input_file_path = os.environ.get('TEMP_FILES_PATH') + "/" + analysis.hash + ".in"
    output_file_path = os.environ.get('TEMP_FILES_PATH') + "/" + analysis.hash + ".out"
    f = open(input_file_path, "w+")
    f.write(input_data)
    f.close()

    command_to_execute = "analysis_tools/alignment/muscle/muscle3.8.31_i86linux64"

    command_to_execute += " -in " + str(input_file_path) + " -out " + str(output_file_path)

    if maketree == "true":
        command_to_execute += " -maketree"
        if maketree_format == "NeighborJoining":
            command_to_execute += " -cluster neighborjoining"
    elif output_format == "clw":
        command_to_execute += " -clw"
    elif output_format == "html":
        command_to_execute += " -html"
    elif output_format == "phys":
        command_to_execute += " -phys"
    elif output_format == "phyi":
        command_to_execute += " -phyi"
    elif output_format == "msf":
        command_to_execute += " -msf"

    result = subprocess.run([command_to_execute], stdout=subprocess.PIPE, shell=True)
    try:
        if int(result.returncode) == 0:
            f = open(output_file_path, "r")
            output_data = f.read()
            analysis.output = output_data
            analysis.status = AnalysisStatus.COMPLETED
            db.session.add(analysis)
            db.session.commit()
        else:
            analysis.status = AnalysisStatus.FAILED
            db.session.add(analysis)
            db.session.commit()
            analysis_status = False
    except Exception as e:
        analysis.status = AnalysisStatus.FAILED
        db.session.add(analysis)
        db.session.commit()
        analysis_status = False

    os.remove(input_file_path)
    os.remove(output_file_path)

    response = {
        'status': analysis_status,
        'output_data': output_data
    }
    return jsonify(**response)


@controller.route('/mafft')
def mafft():
    return render_template('alignment/mafft.html')


@controller.route('/mafft-analysis', methods=["POST"])
def mafft_analysis():
    input_data = request.form['input_data']
    output_format = request.form['output_format']
    output_data = ""
    analysis_status = True

    analysis = Analysis()
    analysis.input = input_data
    analysis.status = AnalysisStatus.IN_PROGRESS
    analysis.type = AnalysisTypes.MAFFT
    analysis.created_on = general.get_current_datetime_for_db()
    if current_user.is_authenticated:
        analysis.user_id = current_user.get_id()

    hash_text = str(time()) + "-" + str(random.randint(0, 100000))
    analysis.hash = hashlib.sha224(hash_text.encode('utf-8')).hexdigest()

    db.session.add(analysis)
    db.session.commit()

    input_file_path = os.environ.get('TEMP_FILES_PATH') + "/" + analysis.hash + ".in"
    output_file_path = os.environ.get('TEMP_FILES_PATH') + "/" + analysis.hash + ".out"
    f = open(input_file_path, "w+")
    f.write(input_data)
    f.close()

    command_to_execute = "analysis_tools/alignment/mafft-linux64/mafft.bat"

    if output_format == "clustalw":
        command_to_execute += " --clustalout"

    command_to_execute += " " + str(input_file_path) + " > " + str(output_file_path)

    result = subprocess.run([command_to_execute], stdout=subprocess.PIPE, shell=True)
    try:
        if int(result.returncode) == 0:
            f = open(output_file_path, "r")
            output_data = f.read()
            analysis.output = output_data
            analysis.status = AnalysisStatus.COMPLETED
            db.session.add(analysis)
            db.session.commit()
        else:
            analysis.status = AnalysisStatus.FAILED
            db.session.add(analysis)
            db.session.commit()
            analysis_status = False
    except Exception as e:
        analysis.status = AnalysisStatus.FAILED
        db.session.add(analysis)
        db.session.commit()
        analysis_status = False

    os.remove(input_file_path)
    os.remove(output_file_path)

    response = {
        'status': analysis_status,
        'output_data': output_data
    }
    return jsonify(**response)
