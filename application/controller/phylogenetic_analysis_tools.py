# Created using PyCharm
# Author: Kamran Akram
# Email: kamranakram1000@live.com
# Date: 5/9/19
import hashlib
import json
import os
import random
import subprocess
from time import time

from flask import Blueprint, render_template, request, jsonify
from flask_login import current_user

from application import db
from application.entity.Analysis import Analysis
from application.utilities import general
from application.utilities.analysis import AnalysisStatus, AnalysisTypes

controller = Blueprint('phylogenetic', __name__, template_folder='templates', url_prefix="/phylogenetic")


@controller.route('/dnapars')
def dnapars():
    return render_template('phylogenetic/dnapars.html')


@controller.route('/dnapars-analysis', methods=["POST"])
def dnapars_analysis():
    input_data = request.form['input_data']
    output_data = ""
    output_tree = ""
    analysis_status = True

    analysis = Analysis()
    analysis.input = input_data
    analysis.status = AnalysisStatus.IN_PROGRESS
    analysis.type = AnalysisTypes.DNAPARS
    analysis.created_on = general.get_current_datetime_for_db()
    if current_user.is_authenticated:
        analysis.user_id = current_user.get_id()

    hash_text = str(time()) + "-" + str(random.randint(0, 100000))
    analysis.hash = hashlib.sha224(hash_text.encode('utf-8')).hexdigest()

    db.session.add(analysis)
    db.session.commit()

    input_file_path = os.environ.get('TEMP_FILES_PATH') + "/" + analysis.hash + ".txt"
    output_data_file_path = os.environ.get('TEMP_FILES_PATH') + "/outfile"
    output_tree_file_path = os.environ.get('TEMP_FILES_PATH') + "/outtree"

    if os.path.exists(output_data_file_path):
        os.remove(output_data_file_path)
    if os.path.exists(output_tree_file_path):
        os.remove(output_tree_file_path)

    f = open(input_file_path, "w+")
    f.write(input_data)
    f.close()

    command_to_execute = "cd " + os.environ.get(
        'TEMP_FILES_PATH') + " && printf '" + analysis.hash + ".txt\\ny' | ../../analysis_tools/phylogenetic/phylip/dnapars"
    result = subprocess.run([command_to_execute], stdout=subprocess.PIPE, shell=True)

    try:
        if result.returncode == 0:
            f = open(output_data_file_path, "r")
            output_data = f.read()
            output_data = output_data.replace('\n\n', '\n')
            output_data = output_data.replace('\n\n', '\n')
            full_output = {
                'Output Data': output_data,
                'Output Tree': ""
            }
            f.close()
            f = open(output_tree_file_path, "r")
            output_tree = f.read()
            full_output['Output Tree'] = output_tree
            analysis.output = json.dumps(full_output)
            analysis.status = AnalysisStatus.COMPLETED
            db.session.add(analysis)
            db.session.commit()
        else:
            analysis.status = AnalysisStatus.FAILED
            db.session.add(analysis)
            db.session.commit()
            analysis_status = False
    except Exception as e:
        analysis.status = AnalysisStatus.FAILED
        db.session.add(analysis)
        db.session.commit()
        analysis_status = False

    os.remove(input_file_path)
    os.remove(output_data_file_path)
    os.remove(output_tree_file_path)

    response = {
        'status': analysis_status,
        'output_data': output_data,
        'output_tree': output_tree
    }
    return jsonify(**response)


@controller.route('/dnaml')
def dnaml():
    return render_template('phylogenetic/dnaml.html')


@controller.route('/dnaml-analysis', methods=["POST"])
def dnaml_analysis():
    input_data = request.form['input_data']
    output_data = ""
    output_tree = ""
    analysis_status = True

    analysis = Analysis()
    analysis.input = input_data
    analysis.status = AnalysisStatus.IN_PROGRESS
    analysis.type = AnalysisTypes.DNAML
    analysis.created_on = general.get_current_datetime_for_db()
    if current_user.is_authenticated:
        analysis.user_id = current_user.get_id()

    hash_text = str(time()) + "-" + str(random.randint(0, 100000))
    analysis.hash = hashlib.sha224(hash_text.encode('utf-8')).hexdigest()

    db.session.add(analysis)
    db.session.commit()

    input_file_path = os.environ.get('TEMP_FILES_PATH') + "/" + analysis.hash + ".txt"
    output_data_file_path = os.environ.get('TEMP_FILES_PATH') + "/outfile"
    output_tree_file_path = os.environ.get('TEMP_FILES_PATH') + "/outtree"

    if os.path.exists(output_data_file_path):
        os.remove(output_data_file_path)
    if os.path.exists(output_tree_file_path):
        os.remove(output_tree_file_path)

    f = open(input_file_path, "w+")
    f.write(input_data)
    f.close()

    command_to_execute = "cd " + os.environ.get(
        'TEMP_FILES_PATH') + " && printf '" + analysis.hash + ".txt\\ny' | ../../analysis_tools/phylogenetic/phylip/dnaml"
    result = subprocess.run([command_to_execute], stdout=subprocess.PIPE, shell=True)

    try:
        if result.returncode == 0:
            f = open(output_data_file_path, "r")
            output_data = f.read()
            output_data = output_data.replace('\n\n', '\n')
            output_data = output_data.replace('\n\n', '\n')
            full_output = {
                'Output Data': output_data,
                'Output Tree': ""
            }
            f.close()
            f = open(output_tree_file_path, "r")
            output_tree = f.read()
            full_output['Output Tree'] = output_tree
            analysis.output = json.dumps(full_output)
            analysis.status = AnalysisStatus.COMPLETED
            db.session.add(analysis)
            db.session.commit()
        else:
            analysis.status = AnalysisStatus.FAILED
            db.session.add(analysis)
            db.session.commit()
            analysis_status = False
    except Exception as e:
        analysis.status = AnalysisStatus.FAILED
        db.session.add(analysis)
        db.session.commit()
        analysis_status = False

    os.remove(input_file_path)
    os.remove(output_data_file_path)
    os.remove(output_tree_file_path)

    response = {
        'status': analysis_status,
        'output_data': output_data,
        'output_tree': output_tree
    }
    return jsonify(**response)


@controller.route('/proml')
def proml():
    return render_template('phylogenetic/proml.html')


@controller.route('/proml-analysis', methods=["POST"])
def proml_analysis():
    input_data = request.form['input_data']
    output_data = ""
    output_tree = ""
    analysis_status = True

    analysis = Analysis()
    analysis.input = input_data
    analysis.status = AnalysisStatus.IN_PROGRESS
    analysis.type = AnalysisTypes.PROML
    analysis.created_on = general.get_current_datetime_for_db()
    if current_user.is_authenticated:
        analysis.user_id = current_user.get_id()

    hash_text = str(time()) + "-" + str(random.randint(0, 100000))
    analysis.hash = hashlib.sha224(hash_text.encode('utf-8')).hexdigest()

    db.session.add(analysis)
    db.session.commit()

    input_file_path = os.environ.get('TEMP_FILES_PATH') + "/" + analysis.hash + ".txt"
    output_data_file_path = os.environ.get('TEMP_FILES_PATH') + "/outfile"
    output_tree_file_path = os.environ.get('TEMP_FILES_PATH') + "/outtree"

    if os.path.exists(output_data_file_path):
        os.remove(output_data_file_path)
    if os.path.exists(output_tree_file_path):
        os.remove(output_tree_file_path)

    f = open(input_file_path, "w+")
    f.write(input_data)
    f.close()

    command_to_execute = "cd " + os.environ.get(
        'TEMP_FILES_PATH') + " && printf '" + analysis.hash + ".txt\\ny' | ../../analysis_tools/phylogenetic/phylip/proml"
    result = subprocess.run([command_to_execute], stdout=subprocess.PIPE, shell=True)

    try:
        if result.returncode == 0:
            f = open(output_data_file_path, "r")
            output_data = f.read()
            output_data = output_data.replace('\n\n', '\n')
            output_data = output_data.replace('\n\n', '\n')
            full_output = {
                'Output Data': output_data,
                'Output Tree': ""
            }
            f.close()
            f = open(output_tree_file_path, "r")
            output_tree = f.read()
            full_output['Output Tree'] = output_tree
            analysis.output = json.dumps(full_output)
            analysis.status = AnalysisStatus.COMPLETED
            db.session.add(analysis)
            db.session.commit()
        else:
            analysis.status = AnalysisStatus.FAILED
            db.session.add(analysis)
            db.session.commit()
            analysis_status = False
    except Exception as e:
        analysis.status = AnalysisStatus.FAILED
        db.session.add(analysis)
        db.session.commit()
        analysis_status = False

    os.remove(input_file_path)
    os.remove(output_data_file_path)
    os.remove(output_tree_file_path)

    response = {
        'status': analysis_status,
        'output_data': output_data,
        'output_tree': output_tree
    }
    return jsonify(**response)


@controller.route('/protpars')
def protpars():
    return render_template('phylogenetic/protpars.html')


@controller.route('/protpars-analysis', methods=["POST"])
def protpars_analysis():
    input_data = request.form['input_data']
    output_data = ""
    output_tree = ""
    analysis_status = True

    analysis = Analysis()
    analysis.input = input_data
    analysis.status = AnalysisStatus.IN_PROGRESS
    analysis.type = AnalysisTypes.PROTPARS
    analysis.created_on = general.get_current_datetime_for_db()
    if current_user.is_authenticated:
        analysis.user_id = current_user.get_id()

    hash_text = str(time()) + "-" + str(random.randint(0, 100000))
    analysis.hash = hashlib.sha224(hash_text.encode('utf-8')).hexdigest()

    db.session.add(analysis)
    db.session.commit()

    input_file_path = os.environ.get('TEMP_FILES_PATH') + "/" + analysis.hash + ".txt"
    output_data_file_path = os.environ.get('TEMP_FILES_PATH') + "/outfile"
    output_tree_file_path = os.environ.get('TEMP_FILES_PATH') + "/outtree"

    if os.path.exists(output_data_file_path):
        os.remove(output_data_file_path)
    if os.path.exists(output_tree_file_path):
        os.remove(output_tree_file_path)

    f = open(input_file_path, "w+")
    f.write(input_data)
    f.close()

    command_to_execute = "cd " + os.environ.get(
        'TEMP_FILES_PATH') + " && printf '" + analysis.hash + ".txt\\ny' | ../../analysis_tools/phylogenetic/phylip/protpars"
    result = subprocess.run([command_to_execute], stdout=subprocess.PIPE, shell=True)

    try:
        if result.returncode == 0:
            f = open(output_data_file_path, "r")
            output_data = f.read()
            output_data = output_data.replace('\n\n', '\n')
            output_data = output_data.replace('\n\n', '\n')
            full_output = {
                'Output Data': output_data,
                'Output Tree': ""
            }
            f.close()
            f = open(output_tree_file_path, "r")
            output_tree = f.read()
            full_output['Output Tree'] = output_tree
            analysis.output = json.dumps(full_output)
            analysis.status = AnalysisStatus.COMPLETED
            db.session.add(analysis)
            db.session.commit()
        else:
            analysis.status = AnalysisStatus.FAILED
            db.session.add(analysis)
            db.session.commit()
            analysis_status = False
    except Exception as e:
        analysis.status = AnalysisStatus.FAILED
        db.session.add(analysis)
        db.session.commit()
        analysis_status = False

    os.remove(input_file_path)
    os.remove(output_data_file_path)
    os.remove(output_tree_file_path)

    response = {
        'status': analysis_status,
        'output_data': output_data,
        'output_tree': output_tree
    }
    return jsonify(**response)
