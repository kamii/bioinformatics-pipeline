# Created using PyCharm
# Author: Kamran Akram
# Email: kamranakram1000@live.com
# Date: 5/9/19

from datetime import datetime
import re
from flask import Blueprint, render_template, request, redirect, url_for
from flask_login import login_user, login_required, logout_user, current_user
from werkzeug.security import generate_password_hash

from application import db, login_manager
from application.entity.Users import Users
from application.utilities import general

controller = Blueprint('user_management', __name__, template_folder='templates', url_prefix="/")


@controller.route('/login', methods=['GET', 'POST'])
def login():
    email = ""
    password = ""

    errors = []

    if current_user.is_authenticated:
        return redirect(url_for('default.index'))

    if request.method == 'POST':

        email = request.form['email']
        password = request.form['password']
        if "remember_me" in request.form:
            remember_me = True
        else:
            remember_me = False

        user = Users.query.filter_by(email=email).first()
        if user:
            if user.check_password(password=password):
                login_user(user, remember=remember_me)

                user.last_login = general.get_current_datetime_for_db()
                db.session.commit()

                referrer = request.form['referrer']
                return redirect(referrer)
            else:
                errors.append("Invalid email or password.")
    return render_template('user-management/login.html',
                           email=email,
                           errors=errors
                           )


@controller.route('/register', methods=['GET', 'POST'])
def register():
    first_name = ""
    last_name = ""
    email = ""
    password = ""
    confirm_password = ""
    errors = []

    if request.method == 'POST':

        first_name = request.form['first_name']
        last_name = request.form['last_name']
        email = request.form['email']

        password = request.form['password']
        confirm_password = request.form['confirm_password']

        if len(password) < 6:
            errors.append("Password must have at least 6 characters.")
        if password != confirm_password:
            errors.append("Passwords do not match.")

        email_regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
        if not re.search(email_regex, email):
            errors.append("Email is not valid.")

        user = Users.query.filter_by(
            email=email).first()

        if user:
            errors.append("Email is already registered.")

        if len(errors) is 0:

            user = Users(first_name=first_name,
                         last_name=last_name,
                         email=email,
                         password=generate_password_hash(password, method='sha256'),
                         created_on=general.get_current_datetime_for_db(),
                         last_login=general.get_current_datetime_for_db()
                         )
            db.session.add(user)
            db.session.commit()
            login_user(user)
            return redirect(url_for('default.index'))
    return render_template('user-management/register.html',
                           first_name=first_name,
                           last_name=last_name,
                           email=email,
                           errors=errors)


@controller.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('user_management.login'))


@login_manager.user_loader
def load_user(user_id):
    if user_id is not None:
        return Users.query.get(user_id)
    return None


@login_manager.unauthorized_handler
def unauthorized():
    return redirect(url_for('user_management.login'))
