# Bioinformatics-Pipeline

A python falsk app for various bioinformatics analysis tools.

### Installation

<ul>
    <li>Clone this repository</li>
    <li>
        Open a terminal, <code>cd</code> into the cloned directory
        <pre><code>cd bioinformatics-pipeline</code></pre>
    </li>
    <li>
        Create a copy of *.dist files without .dist extension.
        <pre><code>cp config/parameters.yaml.dist config/parameters.yaml</code></pre>
        <pre><code>cp .env.dist .env</code></pre>
    </li>
    <li>
        Create and activate python3 virtual environment
        <pre><code>python3 -m venv venv</code></pre>
        <pre><code>source venv/bin/activate</code></pre>
    </li>
    <li>
        Install the required python packages
        <pre><code>pip install -r requirements.txt</code></pre>
    </li>
    <li>
        Install the required npm packages
        <pre><code>npm install</code></pre>
    </li>
    <li>
        Build webpack
        <pre><code>npm run build</code></pre>
    </li>
    <li>
        Run Flask app
        <pre><code>./start.sh</code></pre>
        OR
        <pre><code>python wsgi.py</code></pre>
    </li>
    <li>
        Open in your browser <a href="http://127.0.0.1:5000/">http://127.0.0.1:5000/</a>
    </li>
</ul>

### Development

<ul>
    <li>There is a watch script included for webpack
    <pre><code>npm run watch</code></pre>
    </li>
</ul>
